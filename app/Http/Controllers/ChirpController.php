<?php

namespace App\Http\Controllers;

use App\Models\Chirp;
use Illuminate\Http\Request;

class ChirpController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('chirps.index',[
            //'chirps'=> Chirp::orderBy('created_at','desc')->get()
            'chirps'=> Chirp::with('user')->latest()->get()//al igual que orderby desc, latest hace lo mismo
        ]);
    }





    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //return request('message');
        //podemos hacer un return con requestpara conocer el valor que se manda

        //$message = request('message');
        $validate=$request->validate([
            //'message'=>'required'
            'message'=>['required','min:3', 'max:255']
        ]);
        Auth()->user()->chirps()->create($validate);

        return redirect()->back()->with("status", __('Chirps created successfully!')); //redirecciona a la misma ruta
    }

    /**
     * Display the specified resource.
     */
    public function show(Chirp $chirp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Chirp $chirp)
    {
        // if (auth()->user()->isNot($chirp->user)){

        //     abort(403);

        // }
        $this->authorize('update',$chirp);

        return view(
            'chirps.edit',
            ['chirp'=>$chirp]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Chirp $chirp)
    {
        // if (auth()->user()->isNot($chirp->user)){

        //     abort(403);

        // }
        $this->authorize('update',$chirp);
        $validated=$request->validate([
            'message'=>['required','min:3', 'max:255']
        ]);
        $chirp->update($validated);//se actualiza con el array que se valida, en este caso solo el message
        return to_route('chirps.index')->with("status", __('Chirp updated successfully!'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Chirp $chirp)
    {
        $this->authorize('delete', $chirp);//

        $chirp->delete();
        return to_route('chirps.index')->with("status", __('Chirp deleted successfully!'));
    }
}
